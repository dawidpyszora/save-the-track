package com.example.dawid.savethetrack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class RegisterActivity extends AppCompatActivity {

    private Button registerButton;
    private Button cancelButton;

    private EditText name;
    private EditText surname;
    private EditText login;
    private EditText dateOfBitrh;
    private EditText street;
    private EditText postCode;
    private EditText town;
    private EditText country;
    private EditText email;
    private EditText phoneNr;
    private EditText password;
    private EditText repeatPassword;


    private RadioButton woman;
    private RadioButton man;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerButton = (Button) findViewById(R.id.register_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);

        name = (EditText) findViewById(R.id.name_edit_text);
        surname = (EditText) findViewById(R.id.surname_edit_text);
        login = (EditText ) findViewById(R.id.login_edit_text);
        dateOfBitrh = (EditText) findViewById(R.id.date_of_birth_edit_text);
        street = (EditText) findViewById(R.id.street_edit_text);
        postCode = (EditText) findViewById(R.id.post_code_edit_text);
        town = (EditText) findViewById(R.id.town_edit_text);
        country = (EditText) findViewById(R.id.country_edit_text);
        email = (EditText) findViewById(R.id.email_edit_text);
        phoneNr = (EditText) findViewById(R.id.phone_nr_edit_text);
        password = (EditText) findViewById(R.id.password_edit_text);
        repeatPassword = (EditText) findViewById(R.id.repeat_password_edit_text);

        woman = (RadioButton) findViewById(R.id.woman_radio_button);
        man = (RadioButton) findViewById(R.id.man_radio_button);


        createNewUser();
        dontCreateNewUser();
    }

    private void dontCreateNewUser() {
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void createNewUser() {
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
