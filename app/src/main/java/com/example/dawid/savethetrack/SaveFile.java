package com.example.dawid.savethetrack;

import android.os.Environment;

/**
 * Created by Dawid on 2017-04-23.
 */

public class SaveFile {

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

}