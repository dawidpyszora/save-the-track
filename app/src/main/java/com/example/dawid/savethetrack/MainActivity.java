package com.example.dawid.savethetrack;


import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

;


public class MainActivity extends Activity {
    private MapFragment mapFragment;
    private GoogleMap gMap;
    private GoogleApiClient googleApiClient;


    private double curentLatitiude;
    private double curentLongtitiude;

    private double startLatitiude = 54.3479996;
    private double startLongtitiude = 18.5536573;

    private EditText editText;

    private LatLng gdansk = new LatLng(54.3520500, 18.6463700);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);


        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                gMap = googleMap;
                gMap.getUiSettings().setZoomControlsEnabled(true); // true to enable przyciski do zoomowania
                gMap.getUiSettings().setRotateGesturesEnabled(true);
                gMap.setTrafficEnabled(true);

                if (myCurentLocation() ==true){
                    gMap.setMyLocationEnabled(true);
//                                   CameraPosition cameraPosition = new CameraPosition.Builder().target(gdansk).zoom(5).build();
//                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                }
                else startLocation();
            }
        });

    }
    private void startLocation (){

        gMap.addMarker(new MarkerOptions().position(gdansk).title("gdansk"));
    }

    private boolean  myCurentLocation () {
        return true;
    }

    private void saveFile (){

    }

    private void conectInternet (){}

}
