package com.example.dawid.savethetrack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private Button registerBut;
    private Button loginBut;

    private EditText login;
    private EditText password;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        ButterKnife.bind(this);


        registerBut = (Button) findViewById(R.id.register_button);
        loginBut = (Button) findViewById(R.id.login_button);

        login = (EditText) findViewById(R.id.login_edit_text);
        password = (EditText) findViewById(R.id.password_edit_text);


        goToRegisterUser();
        loginUser();

    }

    private void goToRegisterUser() {
        registerBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class );
                startActivity(intent);

            }
        });
    }
    private void loginUser() {
        loginBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLoginAndPassword();

            }
        });


    }

    private void checkLoginAndPassword(){
        if ( login.getText().toString().equals("Dawid") && password.getText().toString().equals("dawid1")){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
            Toast.makeText(getApplicationContext(),"WELCOME DAWID", Toast.LENGTH_LONG).show();
        }
        else
        Toast.makeText(getApplicationContext(),"Wrong login or password, tray again", Toast.LENGTH_LONG).show();
    }

}
